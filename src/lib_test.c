#include <limits.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <google/cmockery.h>

int add(int a, int b);

void test_add_normal_behaviour (void **state) {
  assert_int_equal(512, add(200, 312));
}

void test_add_with_bing_ints (void **state) {
  assert_int_equal(INT_MAX, add((INT_MAX - 1), 1));
}

void test_add_with_negative_numbers (void **state) {
  assert_int_equal(-5, add(-3, -2));
}

void test_add_with_floats (void **state) {
  assert_int_equal(5.5, add(2.1, 3.4));
}

int main(int argc, const char *argv[])
{
  const UnitTest tests[] = {
    unit_test(test_add_normal_behaviour),
    unit_test(test_add_with_bing_ints),
    unit_test(test_add_with_negative_numbers),
    unit_test(test_add_with_floats)
  }; 
  return run_tests(tests);
}
