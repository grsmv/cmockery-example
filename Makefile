targets = test run_test

all: $(targets)

get-deps: src 
	./dependencies.sh

test: src/lib_test.c
	gcc -Wall -pedantic src/*.c -lcmockery -o test

run_test: test
	./test
