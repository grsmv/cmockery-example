#!/usr/bin/env bash

install_packages ()
{
  local packages=""
  for arg in $@; do local packages="$packages $arg"; done
  apt-get install --force-yes -y $packages
  apt-get update
}

install_dependencies ()
{
  install_packages python-software-properties
  apt-add-repository -y ppa:rubybots/ppa && apt-get update
  install_packages libmongo libbson libcmockery0 libcmockery-dev
}

install_dependencies  > /dev/null 2>&1
case $? in
  0) echo "Dependencies installed successfully";;
  1) echo "Dependencies install failed" && exit 1
esac